const  path = require('path')
const car = {name: "Gentra", color: 'Black', year: 2024}
const carFunc = () => console.log('Car ' + car.name + ' topildi!' + " rangi - " + car.color)
const carUrl = path.parse(__filename)
module.exports = {car, carFunc, carUrl}