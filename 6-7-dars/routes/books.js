const router = require('express').Router()
const books = require('../db/Books')
const uuid = require('uuid')

router.get('/', (req, res) => {
    res.json(books)
})

router.get('/:id', (req, res) => {
    const isExist = books.some(book => book.id === parseInt(req.params.id))
    if (isExist) {
        res.json(books.filter(book => book.id === parseInt(req.params.id)))
    } else {
        res.status(404).json({message: `Siz so'ragan ${req.params.id} - ID lik kitob topilmadi!`})
    }
})

router.post('/', (req, res) => {
    const newBook = {
        id: books.length + 1,
        name: req.body.name,
        author: req.body.author,
        page: req.body.page
    }
    if (!req.body.name || !req.body.author || !req.body.page) {
        res.status(400).json({massage: "Iltimos ma'lumotlarni to'liq kiriting!"})
    } else {
        books.push(newBook)
        res.json(books)
    }
})

router.put('/:id', (req, res) => {
    const isExist = books.some(book => book.id === parseInt(req.params.id))
    if (isExist) {
        const index = books.findIndex(book => book.id === parseInt(req.params.id))
        books[index].name = req.body.name
        books[index].author = req.body.author
        books[index].page = req.body.page
        res.json({
            message: books[index].name + ' nomli Kitob o`zgartirildi!',
            books
        })
    } else {
        res.status(404).json({message: `Siz so'ragan ${req.params.id} - ID lik kitob topilmadi!`})
    }
})

router.delete('/:id', (req, res) => {
    const isExist = books.some(book => book.id === parseInt(req.params.id))
    if (isExist) {
        const index = books.findIndex(book => book.id === parseInt(req.params.id))
        books.splice(index, 1)
        res.json({
            message: 'Kitob o`chirildi!',
            books
        })
    } else {
        res.status(404).json({message: `Siz so'ragan ${req.params.id} - ID lik kitob topilmadi!`})
    }
})

module.exports = router
