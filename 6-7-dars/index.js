const express = require('express')
require('dotenv').config()
const app = express()
const port = process.env.PORT || 8080

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use('/api/books', require('./routes/books'))
app.listen(port, () => {console.log(`Server ${port} portda ishga tushdi!`)})