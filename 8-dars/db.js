const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/dasturlash-5',
    {
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    }
).then(r => console.log('Mongodb muvofaqqiyatli ulandi!', r))
    .catch(err => console.log('DB connection error: ', err))


module.exports = mongoose;