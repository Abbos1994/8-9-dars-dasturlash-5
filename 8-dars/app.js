require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const app = express();
app.use(express.json());
const port = process.env.PORT || 8080;
const mongoDBURL = process.env.DB_URL


mongoose.connect(mongoDBURL,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('DB ulandi!'))
    .catch(err => console.log('DB  error: ', err))

app.listen(port, () => {console.log(`Dasturimiz ${port} portida ishga tushdi!`)})